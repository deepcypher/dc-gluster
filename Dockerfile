FROM archlinux:latest

# resetting all gnupg keys
RUN rm -rf /etc/pacman.d/gnupg && \
    pacman-key --init && \
    pacman-key --populate archlinux && \
    pacman -Qi | gawk '/^Name/ { x = $3 }; /^Installed Size/ { sub(/Installed Size  *:/, ""); print x":" $0 }' | sort -k2,3rn | head -n10 | gawk 'BEGIN { print "\n""These are your 10 largest programs:""\n" }; { print }' && \
    echo "Initial number of packages: $(pacman -Q | wc -l)"


# resync all packages and check keyring
RUN pacman -Syyuu \
    archlinux-keyring \
    base-devel \
    --noconfirm

# installing subsequent packages not related to base system
# https://wiki.archlinux.org/title/GlusterFS
RUN pacman -S git glusterfs --noconfirm

# variable for user username to use in the container
ARG user_name=archie

# variable for user password to use in the container
ARG user_password=archer

# creating user with the desired permissions (NOPASS required for pikaur stages)
RUN useradd -m -p $(openssl passwd -1 ${user_password}) ${user_name} && \
    echo "${user_name} ALL=(ALL) ALL" >> /etc/sudoers.d/${user_name} && \
    echo "${user_name} ALL=(ALL) NOPASSWD:/usr/bin/pacman" >> /etc/sudoers.d/${user_name} && \
    echo "exec fish" >> /root/.bashrc

# swapping to our newly created user
USER ${user_name}

# clone, build, and install pikaur
RUN mkdir /home/${user_name}/git && \
    cd /home/${user_name}/git && \
    # git clone "https://github.com/actionless/pikaur" && \
    git clone "https://aur.archlinux.org/pikaur.git" && \
    cd /home/${user_name}/git/pikaur && \
    makepkg -s --noconfirm && \
    echo "${user_password}" | sudo -S pacman -U *pkg.tar.zst --noconfirm

# swapping back to root to continue since we no longer desire to be a user for makepkg
USER root

# install more specific packages from community and AUR as needed, E.G some in-container libs to aid development/ testing:
# RUN sudo -u ${user_name} pikaur -S --noconfirm SOME-AUR-PACKAGE --noconfirm

# cleaning up now uneeded junk, its served its purpose to get us here now
RUN pacman -Rsn pikaur git sudo --noconfirm && \
    userdel --remove --force ${user_name} && \
    rm /etc/sudoers.d/${user_name} && \
    pacman -Scc && \
    useradd ${user_name} && \
    mkdir -p /data && \
    chmod -R 700 /data && \
    chown -R ${user_name}:${user_name} /data && \
    pacman -Qi | gawk '/^Name/ { x = $3 }; /^Installed Size/ { sub(/Installed Size  *:/, ""); print x":" $0 }' | sort -k2,3rn | head -n10 | gawk 'BEGIN { print "\n""These are your 10 largest programs:""\n" }; { print }' && \
    echo "Number of packages: $(pacman -Q | wc -l)"

# now swap back to this wiped user
# USER ${user_name}
# actually quite difficult to get all permissions of glusterd to run under a user

# ENTRYPOINT ["/usr/bin/bash", "-c"]

# making this CMD so we can override in a neater way than if it was an entrypoint
CMD ["/usr/sbin/glusterd", "-p", "/var/run/glusterd", "--no-daemon", "--log-file", "/dev/stdout"]
