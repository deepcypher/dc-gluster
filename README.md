# dc-gluster

Experimental in-kube file-system layer.

Plan: Mount local volumes automagically on specified nodes, and to specific paths. Then host a GlusterFS between these pods/ containers to replicate and manage the storage across all these volumes, without having to do it out of cluster AND getting all the nice things from within the cluster, I.E cert-manager <3
